<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArticleController@index')->name('article.index');

Route::get('create', 'ArticleController@create')->name('create.article');

Route::post('store', 'ArticleController@Store')->name('article.store');

Route::get('edit/article/{id}','ArticleController@Edit');
Route::get('show/article/{id}','ArticleController@Show');
Route::post('update/article/{id}','ArticleController@Update');
