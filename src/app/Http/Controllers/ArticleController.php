<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use DB;

class ArticleController extends Controller
{
    public function index() {

        $article = DB::table('articles')->latest()->paginate(3);

        return view('index',compact('article'));
    }

    public function create() {
        return view('article.create');
    }

    public function Store(Request $request) {

        $data = array();
        $data['title'] = $request->title;
        $data['body'] = $request->body;

        $image = $request->file('image');
        if ($image) {
            $image_name = date('ymd-H.i.s');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);
            $data['image'] = $image_url;
            $article = DB::table('articles')->insert($data);
            return redirect()->route('article.index')
                ->with('success', 'Article Created Successfully');
        }
    }

    public function Edit($id) {

        $article = DB::table('articles')->where('id',$id)->first();
        return view('article.edit',compact('article'));
    }

    public function Update(Request $request, $id) {
        $oldimage = $request->old_image;

        $data = array();
        $data['title'] = $request->title;
        $data['body'] = $request->body;

        $image = $request->file('image');
        if ($image) {
            if($oldimage){
                unlink($oldimage);
            }
            $image_name = date('ymd-H.i.s');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);
            $data['image'] = $image_url;
            $article = DB::table('articles')->where('id', $id)->update($data);
            return redirect()->route('article.index')
                ->with('success', 'Article Updated Successfully');
        }
    }

    public function Show($id) {

        $data = DB::table('articles')->where('id', $id)->first();
        return view('article.show',compact('data'));
    }

}
