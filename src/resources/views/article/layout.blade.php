<!DOCTYPE html>
<html>
<head>

    <title>Copenhagen Times</title>

    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
<div class="container">

    @yield('content')

</div>
<script src="/js/app.js"></script>
</body>
</html>
