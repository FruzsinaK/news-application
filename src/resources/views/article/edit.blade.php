@extends('article.layout')

@section('content')
    <br><br>
    <div class="row">
        <div class="col-lg-6 margin-tb">
            <div class="pull-left">
                <h2> Edit Article</h2>
            </div>
        </div>
        <div class="col-lg-6 margin-tb" align="right">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('article.index') }}"> Back </a>
            </div>
        </div>
    </div>
    <br>


    <form action="{{ url('update/article/'.$article->id) }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-xs-9 col-sm-9 col-md-9">
                <div class="form-group">
                    <strong>Title</strong>
                    <input type="text" name="title" class="form-control" value="{{
                $article->title }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Body</strong>
                    <textarea class="form-control" name="body" style="height: 500px" placeholder="Body">
                    {{$article->body }}
                </textarea>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <br><strong>Image:</strong>
                    <input type="file" name="image">
                </div>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Old image:</strong>
                    <img src="{{ URL::to($article->image) }}" height="100px;" width="180px;">
                    <input type="hidden" name="old_image" value="{{$article->image}}">
                </div>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6" align="right">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>

@endsection

