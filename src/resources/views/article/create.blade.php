@extends('article.layout')

@section('content')
    <br><br><br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Add New Article</h2>
            </div>

            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('article.index') }}"> Back </a>
            </div>
        </div>

    </div>

    <form action="{{ route('article.store') }}" method="POST" enctype="multipart/form-data">

        @csrf

        <div class="row">
            <div class="col-xs-9 col-sm-9 col-md-9">
                <div class="form-group">
                    <strong>Article Title:</strong>
                    <input type="text" name="title" class="form-control" placeholder="Title">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Body</strong>
                    <textarea class="form-control" name="body" style="height: 500px" placeholder="Body"></textarea>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Image:</strong>
                    <input type="file" name="image">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>

@endsection
