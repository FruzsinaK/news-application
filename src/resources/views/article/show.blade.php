@extends('article.layout')

@section('content')
    <br><br><br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> {{ $data->title }}</h2>
            </div>


        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <br>
            <div class="form-group">
                <img src="{{ URL::to($data->image) }}" height="300px;" width="500px;">
            </div>
            <br>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{ $data->body }}
            </div>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('article.index') }}"> Back </a>
        </div>
    </div>


@endsection
