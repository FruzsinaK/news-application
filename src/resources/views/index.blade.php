@extends('article.layout')

@section('content')


    <header class="blog-header py-5">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4 pt-1">
                <a class="text-muted" href="#">Subscribe</a>
            </div>
            <div class="col-4 text-center">
                <h1>Copenhagen Times</h1>
            </div>
            <div class="col-4 d-flex justify-content-end align-items-center">
                <a class="btn btn-sm btn-outline-success" href="{{ route('create.article') }}">Create new article</a>
            </div>
        </div>
    </header>


    @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p> {{ $message }} </p>
        </div>
    @endif

    @foreach($article as $ar)
        <div class="row mb-2">
            <div class="col-md-12">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <h3 class="mb-2">{{ $ar->title }}</h3>
                        <p class="card-text mb-auto">{{ str_limit($ar->body, $limit = 200) }}</p>
                        <a href="{{ URL::to('show/article/'.$ar->id) }}" class="btn btn-outline-primary">Continue reading</a>
                        <br>
                        <a href="{{ URL::to('edit/article/'.$ar->id) }}" class="btn btn-outline-secondary">Edit article</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img src="{{ URL::to($ar->image) }}" height="300px;" width="500px;">
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    {!! $article->links() !!}



@endsection
